// 1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json

// 2023-10-13T03:47:17.084+0000	connected to: mongodb://localhost/
// 2023-10-13T03:47:17.115+0000	800 document(s) imported successfully. 0 document(s) failed to import.


// 2)¿Cuántos registros arrojo el comando count?

    db.grades.count()
//  Respuesta: 800

// 3) Encuentra todas las calificaciones del estudiante con el id numero 4.
      db.grades.find({ "student_id": 4 });

//     ***Respuesta***

//             [
//         {
//             _id: ObjectId("50906d7fa3c412bb040eb587"),
//             student_id: 4,
//             type: 'exam',
//             score: 87.89071881934647
//         },
//         {
//             _id: ObjectId("50906d7fa3c412bb040eb589"),
//             student_id: 4,
//             type: 'homework',
//             score: 5.244452510818443
//         },
//         {
//             _id: ObjectId("50906d7fa3c412bb040eb588"),
//             student_id: 4,
//             type: 'quiz',
//             score: 27.29006335059361
//         },
//         {
//             _id: ObjectId("50906d7fa3c412bb040eb58a"),
//             student_id: 4,
//             type: 'homework',
//             score: 28.656451042441
//         }
//         ]


// 4) ¿Cuántos registros hay de tipo exam?

      db.grades.count({type:"exam"});

//     Respuesta:200

// 5) ¿Cuántos registros hay de tipo homework?

     db.grades.count({type:"homework"});

//   Respuesta: 400

// 6) ¿Cuántos registros hay de tipo quiz?

     db.grades.count({type:"quiz"});
//   Respuesta:  200

// 7) Elimina todas las calificaciones del estudiante con el id numero 3

    db.grades.deleteMany({ "student_id": 3 });

    // { acknowledged: true, deletedCount: 4 }

// 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?

    db.grades.find({ "score": 75.29561445722392 });

    // [
    //     {
    //       _id: ObjectId("50906d7fa3c412bb040eb59e"),
    //       student_id: 9,
    //       type: 'homework',
    //       score: 75.29561445722392
    //     }
    //   ]
      


// 9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

    db.grades.updateOne({ "_id": ObjectId("50906d7fa3c412bb040eb591") }, { $set: { "score": 100 } });

    // {
    //     acknowledged: true,
    //     insertedId: null,
    //     matchedCount: 1,
    //     modifiedCount: 0,
    //     upsertedCount: 0
    //   }


// 10) A qué estudiante pertenece esta calificación.

    db.grades.find({ _id: ObjectId("50906d7fa3c412bb040eb591") });
    
 // Respuesta: studen_id = 6 

    // [
    //     {
    //       _id: ObjectId("50906d7fa3c412bb040eb591"),
    //       student_id: 6,
    //       type: 'homework',
    //       score: 100
    //     }
    //   ]




