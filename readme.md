# Tarea | Mongodb 101

El contenido de este repositorio muestra operaciones se llevaron a cabo utilizando comandos de MongoDB siguienndo la siguiente secuencia:

1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json

2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?

3) Encuentra todas las calificaciones del estudiante con el id numero 4.

4) ¿Cuántos registros hay de tipo exam?

5) ¿Cuántos registros hay de tipo homework?

6) ¿Cuántos registros hay de tipo quiz?

7) Elimina todas las calificaciones del estudiante con el id numero 3

8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?

9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

10) A qué estudiante pertenece esta calificación.